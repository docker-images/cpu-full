ARG UBUNTU_MAJOR_VERSION=22
ARG UBUNTU_MINOR_VERSION=04
FROM ubuntu:${UBUNTU_MAJOR_VERSION}.${UBUNTU_MINOR_VERSION}

RUN apt update && apt install -y python3-pip git && apt-get clean
RUN pip3 install --upgrade pip
RUN pip3 install setuptools 
RUN pip3 install jupyterlab
RUN pip3 install tensorflow
RUN pip3 install torchvision torch
RUN pip3 install numpy matplotlib pandas keras scipy pyvista imutils
RUN pip3 install scikit-image scikit-learn
RUN pip3 install opencv-python opencv-contrib-python
RUN pip3 install openpyxl cvxopt datetime dateutils tk

# Use non interactive to bypass tzdata post install
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y openssh-server curl && apt-get clean

# pre-create /run/sshd
RUN service ssh start

RUN curl -fsSL https://code-server.dev/install.sh | sh

# Opencv dependencies
RUN apt update && apt install -y ffmpeg libsm6 libxext6 && apt-get clean

# Some TF tools expect a "python" binary
RUN ln -s $(which python3) /usr/local/bin/python

ENV TF_FORCE_GPU_ALLOW_GROWTH=true

RUN apt update && apt install -y sudo && apt-get clean
 